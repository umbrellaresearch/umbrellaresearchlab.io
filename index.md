---
layout: home
#title: "Yoga.Dev"
---

<style>
.rightlogo {
  float: right;
  margin-left: 30px;
}
</style>
<img src="/images/appicon.png" width="170" height="170" class="rightlogo">

Yoga Studio is an open-source development environment that helps you test and debug code live on robots.

The app displays your code with numeric parameters overlaid with draggable sliders. You can modify parameters in real time and watch the robot’s behavior change. It makes the tuning process much faster.

To try it, clone the [repo](https://gitlab.com/umbrellaresearch/yoga2) and follow the instructions in the readme.

<br clear=all>

For how to write your own robot control programs, see the [Language Manual](/yoga/).
For a live robot to test on, see our [Timeshared Robots](/timeshared-robots/).
