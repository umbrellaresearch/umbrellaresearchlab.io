---
layout: page
title: About
permalink: /about/
---

<img class="authorpic" src="/images/tlb_gs2_7019_480px.jpg" width="240px" height="160px">

Yoga Studio was started by Trevor Blackwell as a better way to program robots to do useful things.

Previously, he was a co-founder and partner with [Y Combinator](https://www.ycombinator.com). Before that he founded Anybots, which developed telepresence robots. Before that, he was a co-founder of Viaweb, which made end-user e-commerce software. He has a PhD in computer science from Harvard University.
