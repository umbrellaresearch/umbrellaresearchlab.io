---
layout: post
title:  "Yoga Implementation"
---

Yoga is a functional programming language for real-time robot control,
and [Yoga Studio](https://gitlab.com/umbrellaresearch/yoga2) is an open-source
development environment that helps you test and debug code live on robots.

Early prototypes of Yoga were written in Javascript and emitted C++ code, which was then compiled.
This was a convenient way to prototype, but rebuilding the C++ code was incredibly slow,
over a minute for a 500-line code base. Yoga now includes a complete compiler written in C++,
using the LLVM code generator. It can compile the same code base in about a second.



 