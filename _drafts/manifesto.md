---
layout: page
title: "Yoga Studio Manifesto"
permalink: /manifesto/
---

<img src="/images/rosenschein-slide.jpg" width="240" height="163" class="slideright">

Why is it so hard to program robots? The slide at right (from a talk by Stan Rosenschein) proposes one answer.

You could prove this by actually finding such a theory, and using it
to quickly program some general-purpose robots. A number of people are working on this,
but Yoga Studio is trying something different.

<br clear=all>

Yoga Studio's answer is: "We need better developer tools". There are two things I prefer about this answer:

* There are examples of this in other domains. For example, I think video games evolved from Doom to Fortnite
more due to better tools than a better unified theory.
* I like building tools.

<img src="/images/spotwalking.jpg" width="256" height="144" class="slideright">

We've seen robots do impressive things, after a team of control theory PhDs have spent
years on it. But because doing each new thing takes so long, we've never seen a robot do more than a few things.

This is very slow compared to video games, where a programmer can add a new NPC behavior every day.

<br clear=all>

## The Parameter Problem

Any robot control algorithm can be written as:

<div class="pseudocode">
  motor_commands[t+1], state[t+1] = f(sensor_inputs[t], state[t])
</div>


