---
layout: page
title: "Yoga Studio"
---

Yoga Studio is an open-source development environment that helps you test and debug code live on robots. You can try it out using the code for greppy (a balancing robot) with:

```sh
yoga2 $ bin/yogastudio examples/greppy/main.yoga
```

The UI should start with:

<img src="/images/studiofrontpage@2x.png" width="582" height="174">

Click "Run live on on robot greppy1". Unless you've configured it with your own robot, it will connect to a robot in our California warehouse. (It's time-shared, so you might have to wait for someone else to finish.) 

It'll then open up the real-time scope, showing live-scrolling graphs of several variables over time. And you'll see the robot starting to balance in the upper right corner.

<img src="/images/ScreenshotAnno@2x.png" width="740" height="485">

The greppy example code lets you drive the robot from your keyboard with WASD.

The app displays your code with numeric parameters overlaid with draggable sliders. When you drag
a parameter value, it sends it to the robot and it starts running the new behavior immediately.

<table class="side-by-side">
  <thead>
    <tr>
      <td class="th-center"> Regular Editor </td>
      <td class="th-center"> Yoga Studio </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <img src="/images/paramdemo_raw.png" width="331" height="123" alt="Parameters as text">
      </td>
      <td>
        <img src="/images/paramdemo_yoga.gif" width="316" height="122" alt="Parameters in Yoga Studio">
      </td>
    </tr>
  </tbody>
</table>

Hit Esc to end the live session, and switch to the recorded traces it just captured.

