# TODO

* Quick 5-sec video showing greppy from all angles. Better than a picture.


* "Like autotune for your control loop parameters"?



## Video outline

* Yoga & Yoga Studio
* Trying solve the parameter problem
  * Every new language should solve a specific problem.
    * Rust: how do we get C performance with memory safety?
    * Yoga: how do we get 




## Video dialog

Let me show you a demo of Yoga Studio.

It's for running robot. So lets start by running live on a robot.
Now it's connected to this robot in our California warehouse.
You can drive it interactively with the keyboard.
It's recording all the values that are generated in the code.
Here's the code that's making it balance.
This line computes the acceleration as a function of the tilt angle.
Later we convert that to a velocity and sent it to the wheels.

This is running live. And we can change the parameters live.
If we do this [reduce accel from vel] it'll become unstable and start wobbling.

And we'll turn it back up before it falls over.
This lets you turn parameters in real time.
Also, you can exit out of live mode.
And it'll load high-resolution traces.

[zoom out]

Now we're looking at the recorded traces.
We can scrub backwards and forwards in time.
Here's the period where we changed the feedback parameter and it's not balancing very well.

Also here we can change the parameters, and in recorded mode it does something different.
It shows what the system would have done with the same inputs but new parameters.
You'l notice there are two sets of lines in the traces.
The faint one is what actually happened during the run.
The bold one is what the code would have done with the latest parameters but the same inputs.

So that's how you can fiddle with parameters by trial and error. But there's another thing
we can do, because Yoga supports backpropagation. We can add a desired change to any of the
outputs of the code, and it'll show us how we could change the parameters to get that change.

Let's click on the commanded wheel velocity, which is an output of the balance code.

[drive.cmd.lVel +ve]

We're basically asking the question: what would it take to make the left wheel velocity number higher.

And it shows us the answer by overlaying fuschia arrows to show what direction we could change the parameter to increase the left wheel velocity.

So for example we could try this one

[targetAngle from speedErr]

and we'll see that the new left wheel velocity is higher.












## Review

* https://www.argmin.net/2020/06/29/tour-revisited/
