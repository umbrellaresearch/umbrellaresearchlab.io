---
layout: post
title:  "Timeshared Robots"
date:   2020-06-29 08:00:00 -0700
permalink: /timeshared-robots/
redirect_from: 
  - /2020/06/29/Open-Access-Robots.html
  - /2020/06/23/Open-Access-Robots.html
  - /2020/06/19/Open-Access-Robots.html
  - /open-access-robots/
---

I've been wanting to do a Show HN for my robotics developer tool [Yoga Studio]({{ site.gitlab_corner }}) for a while.
But given that:
* it's for programming robots
* you probably don't have a compatible robot

it wasn't something you could immediately play with.

<img src="/images/greppy1tlb_300px.jpg" width="150" height="317" class="robotpicright">

Now, you have **remote access to a robot** you can program with it! The 2-wheeled balancing robot ("Greppy") is sitting in
our California warehouse ready to run your code. You'll need to download and build
Yoga Studio to control it -- see full instructions below.

When you check out the code, it includes an `examples/greppy` directory with some code to
make the robot balance and drive around. When you start the app and click "Run Live", it:

* Connects to the robot through a port in our firewall.
* Acquires a lock (only one user at a time. Don't bogart the robot!)
* Uploads **your code** to the robot (start with the example code, but you can hack it up later.)
* The robot compiles and runs your code 
* The robot **streams live traces** of all the sensors and computations done by the code, and also a video from a camera looking at the robot.
* You can **drive the robot** with WASD.
* The bottom pane shows the code running, along with **current values for every expression**.
* In the code pane, numerical parameters are overlaid with gui sliders. **You can drag them around and they're immediately updated on the robot**.
* When you're done, hit Escape and it'll download the high-res traces to your machine.
* While viewing a saved trace, you can adjust parameters and it'll overlay a counterfactual graph of **what the code would have done** with the new parameters.
* The Yoga language is fully **differentiable WRT its parameters** so you can click on an output variable
 (like motor speed) and it'll tell you what parameters you should change in which direction to affect it.

### Motivation

This is part of a project to make programming robots faster. My experience is that tuning the parameters of
robot control code takes 10x longer than getting the structure of the code right, so I designed a
language + IDE with a single-minded focus on solving the parameter problem. The language is:

* Differentiable with respect to its parameters
* Fast (statically typed and compiled)
* Purely functional (no side effects)
* Close enough to C for your editor's syntax highlighting to work.

The compiler extracts anything marked as a parameter into a separate data structure, with a link to the location
in the source code they came from. When the IDE renders the Yoga code, it shows the parameters as sliders.

<table class="side-by-side">
  <thead>
    <tr>
      <td class="th-center"> Code Editor </td>
      <td class="th-center"> Yoga Studio </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <img src="/images/paramdemo_raw.png" width="331" height="123" alt="Parameters as text">
      </td>
      <td>
        <img src="/images/paramdemo_yoga.gif" width="316" height="122" alt="Parameters in Yoga Studio">
      </td>
    </tr>
  </tbody>
</table>

Here's what Yoga Studio looks like when controlling Greppy:

<img src="/images/ScreenshotAnno@2x.png" width="740" height="485">


Feel free to hack on the code in [examples/greppy]({{ site.yoga2_code }}examples/greppy). See if you can make the robot do something cool!

Read more in the [Reference Manual](/yoga/).


# Building and running

It should work on either Mac or Linux (Recently tested on Catalina and Ubuntu 19.10 & 20.04).

```sh
git clone --recurse-submodules https://gitlab.com/umbrellaresearch/yoga2.git
cd yoga2
git config submodule.recurse true
make install.deps # On Mac, uses Brew. On Linux, apt-get.
make -j8
```

Now fire up the Greppy example code:

```sh
bin/yogastudio examples/greppy/main.yoga
```

Click "Run live on on robot greppy1".

It'll then open up the real-time scope, showing live-scrolling graphs of several variables over time. And the video will show the robot starting to balance.

The greppy example code lets you drive the robot from your keyboard with WASD.

Try going to the `balance.yoga` file, and adjusting the first 2 parameters in the RHS of the assignment to `accel`. Those are roughly the P and D parameters of the main balance feedback loop. Start with
small adjustments and work up to large ones! The robot is attached to the ceiling with a rope, so you can't do too much damage.

Hit Esc to end the live session, and switch to the recorded traces it just captured.

You can scroll back and forth in the recorded traces by scrolling left-right with your mouse. You can add or remove traces from the graph with the checkboxes on the left.

While looking at recorded traces, you can still drag parameters, but now rather than updating on the live robot, they **redo the entire run**, overlaying the counterfactual graphs. That is, it shows what the code would have done with the new parameters given the same inputs from the hardware.

To start another run on the hardware, hit Cmd-L (Ctl-L on Linux).

When you're happy with the parameters, click "Save params" to write them back to the source code. If you're following the example, that'll be in the checked out directory under examples/greppy/

Please send any questions to support@yoga.dev
